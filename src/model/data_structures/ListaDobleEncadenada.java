package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements IStack<T>, IQueue<T>
{
	public NodoDoble<T> first = null;
	public NodoDoble<T> last = null;
	public NodoDoble<T> tope = null;
	private int size = 0;

	@Override
	public Iterator<T> iterator() 
	{
		if (size == 0) 
		{
			return null;
		}
		return new Iterator<T>() 
		{
			private NodoDoble<T> currentNode = null;

			@Override
			public boolean hasNext() 
			{
				boolean result = false;
				if(currentNode.darSiguiente( ) == null)
				{
					result=false;
				}
				else if(currentNode.darSiguiente() != null)
				{
					result = true;
				}
				else if(first == null)
				{
					result = false;
				}
				return result;

			}

			@Override
			public T next() 
			{
				if (currentNode == null) {
					currentNode = (NodoDoble<T>) first;
					currentNode = (NodoDoble<T>) last;
					return currentNode.getItem();
				}
				if (currentNode.darSiguiente() == null) 
				{
					try 
					{
						throw new NoExisteElemento("Siguiente de :"+currentNode.toString());
					}
					catch (NoExisteElemento e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					currentNode = currentNode.darSiguiente();
				}
				return currentNode.getItem();
			}
		};
	}

	public NodoDoble<T> darPrimerElemento()
	{
		return first;
	}

	public NodoDoble<T> darUltimoElemento()
	{
		return last;
	}
	
	public boolean expresionBienFormada (String operacion)
	{
		boolean resp = false;
		if(operacion.startsWith("[") && operacion.endsWith("]"))
		{
			resp = true;
		}
		return resp;
	}

	//M�todos de la Interface Queue

	@Override
	public void enqueue(T item) 
	{
		NodoDoble<T> newNodo = new NodoDoble<T>();
		newNodo.setItem(item);

		if(size == 0)
		{
			first = newNodo;
			last = newNodo;
			size++;
		}
		else
		{
			last.setSiguiente(newNodo);
			last = newNodo;
			size++;

		}
	}

	@Override
	public T dequeue() throws NoExisteElemento
	{
		if(size == 0)
		{
			throw new NoExisteElemento("La cola est� vacia");
		}
		else
		{
			first = first.darSiguiente();
			size--;
		}
		return first.getItem();
	}

	@Override
	public boolean isEmptyQueue() 
	{
		return first == null;
	}

	@Override
	public int sizeQueue() 
	{
		return size;
	}

	//M�todos de la interface Stack

	@Override
	public void push(T item) 
	{
		NodoDoble<T> newNodo = new NodoDoble<T>();
		newNodo.setItem(item);

		if(size == 0)
		{
			tope = newNodo;
			size++;
		}
		else
		{
			newNodo.setSiguiente(tope);
			tope = newNodo;
			size++;
		}
	}

	@Override
	public T pop()
	{
		if(tope.darSiguiente() == null)
		{
			return null;
		}
		else
		{
			tope = tope.darSiguiente();
			size--;
			return tope.getItem();
		}
	}

	@Override
	public boolean isEmptyStack() 
	{
		return tope == null;
	}

	@Override
	public int sizeStack() 
	{
		return size;
	}

}