package model.data_structures;

import java.util.Iterator;

public interface IStack<T>
{	
	Iterator<T> iterator();
	
	/**
	 * Agrega un item al tope de la pila
	 * @param elem
	 */
	public void push (T item);
	
	/**
	 * Elimina el elemento en el tope de la pila
	 * @return Retorna el elemento siguiente, que ahora ser� el tope
	 */
	public T pop();
	
	/**
	 * Indica si la cola est� vac�a
	 * @return true si est� vacio, false si la cola contiene elementos
	 */
	public boolean isEmptyStack();
	
	/**
	 * N�mero de elementos en la cola
	 * @return Retorna el tamanio de la cola
	 */
	public int sizeStack();
}
