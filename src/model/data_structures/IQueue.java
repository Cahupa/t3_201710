package model.data_structures;

import java.util.Iterator;

public interface IQueue<T>
{	
	Iterator<T> iterator();

	/**
	 * A�ade el elemento elem al final de la lista
	 * @param elem
	 */
	public void enqueue (T item);
	
	/**
	 * Elimina el elemento de la primera posici�n de la cola
	 * @return Retorna el elemento siguiente, que ahora ser� el ultimo
	 * @throws NoExisteElemento 
	 */
	public T dequeue() throws NoExisteElemento;
	
	/**
	 * Indica si la cola est� vac�a
	 * @return true si est� vacio, false si la cola contiene elementos
	 */
	public boolean isEmptyQueue();
	
	/**
	 * N�mero de elementos en la cola
	 * @return Retorna el tamanio de la cola
	 */
	public int sizeQueue();
}
