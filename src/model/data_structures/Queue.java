package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T>
{
	ListaDobleEncadenada<T> listaDobleEncadenada;

	public Queue()
	{
		listaDobleEncadenada = new ListaDobleEncadenada<T>();
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return listaDobleEncadenada.iterator();
	}

	@Override
	public void enqueue(T item) 
	{
		listaDobleEncadenada.push(item);
	}

	@Override
	public T dequeue()
	{
		return listaDobleEncadenada.pop();
	}

	@Override
	public boolean isEmptyQueue() 
	{
		return listaDobleEncadenada.isEmptyQueue();
	}

	@Override
	public int sizeQueue() 
	{
		return listaDobleEncadenada.sizeQueue();
	}

}