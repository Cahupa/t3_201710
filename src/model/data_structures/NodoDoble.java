package model.data_structures;

public class NodoDoble <T> 
{

	private NodoDoble <T> anterior;
	private NodoDoble <T> siguiente;
	private T item = null;
	
	public NodoDoble()
	{
		siguiente = null;
		anterior = null;
	}

	public NodoDoble<T> darSiguiente()
	{
		return siguiente;

	}

	public NodoDoble<T> darAnterior()
	{
		return anterior;

	}

	public void setSiguiente(NodoDoble<T> siguiente)
	{
		this.siguiente= siguiente;
	}

	public void setAnterior (NodoDoble<T> anterior)
	{
		this.anterior=anterior;
	}
	
	public void setItem(T pItem)
	{
		item = pItem;
	}
	
	public T getItem()
	{
		return item;
	}
}