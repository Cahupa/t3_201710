package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>
{
	ListaDobleEncadenada<T> listaDobleEncadenada;
	
	public void stack()
	{
		listaDobleEncadenada = new ListaDobleEncadenada<T>();
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return listaDobleEncadenada.iterator();
	}

	@Override
	public void push(T item) 
	{
		listaDobleEncadenada.push(item);
	}

	@Override
	public T pop()
	{
		return listaDobleEncadenada.pop();
	}

	@Override
	public boolean isEmptyStack() 
	{
		return listaDobleEncadenada.isEmptyStack();
	}

	@Override
	public int sizeStack() 
	{
		return listaDobleEncadenada.sizeStack();
	}
}
