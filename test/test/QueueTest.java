package test;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.NoExisteElemento;

public class QueueTest extends TestCase
{
	private ListaDobleEncadenada<String> queue;

	private void setUpEscenario1( )
	{
		queue = new ListaDobleEncadenada<String>();
	}

	private void setUpEscenario2( )
	{
		queue = new ListaDobleEncadenada<String>();

		/*0*/ queue.enqueue("Dylan");
		/*1*/ queue.enqueue("Pablo");
		/*2*/ queue.enqueue("Juan Manuel");
		/*3*/ queue.enqueue("Melissa");
		/*4*/ queue.enqueue("laura");
		/*5*/ queue.enqueue("Angie");
	}

	public void testEnqueue()
	{
		setUpEscenario1( );
		queue.enqueue("Carlos");
		assertEquals("El primero deberia ser Carlos", "Carlos", queue.first.getItem());
		assertEquals("El �ltimo deberia ser Carlos", "Carlos", queue.last.getItem());
		
		setUpEscenario2( );
		queue.enqueue("Sara");
		assertEquals("El elemento deberia ser Sara", "Sara", queue.last.getItem());
	}
	
	public void tesDequeue() throws NoExisteElemento
	{
		setUpEscenario2();
		assertEquals("El primer elementro deber�a ser Laura", "Laura", queue.dequeue());
	}

	public void testIsEmptyQueue()
	{
		setUpEscenario1();
		assertEquals("La cola deber�a estar vac�a", true, queue.isEmptyQueue());
	}
	public void testSizeQueue()
	{
		setUpEscenario1( );
		queue.enqueue("Ana");
		assertEquals(1, queue.sizeQueue() );

		setUpEscenario2( );
		assertEquals(6, queue.sizeQueue());
	}
}
