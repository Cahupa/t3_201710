package test;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.NoExisteElemento;

public class StackTest extends TestCase
{
	private ListaDobleEncadenada<String> stack;

	private void setUpEscenario1( )
	{
		stack = new ListaDobleEncadenada<String>();
	}

	private void setUpEscenario2( )
	{
		stack = new ListaDobleEncadenada<String>();

		/*0*/ stack.push("Dylan");
		/*1*/ stack.push("Pablo");
		/*2*/ stack.push("Juan Manuel");
		/*3*/ stack.push("Melissa");
		/*4*/ stack.push("Laura");
		/*5*/ stack.push("Angie");
	}

	public void testPush()
	{
		setUpEscenario1( );
		stack.push("Carlos");
		assertEquals("El primero deberia ser Carlos", "Carlos", stack.tope.getItem());
		
		setUpEscenario2( );
		stack.push("Sara");
		assertEquals("El elemento deberia ser Sara", "Sara", stack.tope.getItem());
	}
	
	public void testPop() throws NoExisteElemento
	{
		setUpEscenario2();
		assertEquals("El primer elementro deber�a ser Laura", "Laura", stack.pop());
	}

	public void testIsEmptyStack()
	{
		setUpEscenario1();
		assertEquals("La pila deber�a estar vac�a", true, stack.isEmptyStack());
	}
	public void testSizeStack()
	{
		setUpEscenario1( );
		stack.push("Ana");
		assertEquals(1, stack.sizeQueue() );

		setUpEscenario2( );
		assertEquals(6, stack.sizeQueue());
	}
}
